<?php

use App\User;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => "Lastmile Networks",
        'email' => "system@lastmile-networks.net",
        'password' => $password ?: $password = bcrypt('System@lastmile'),
        'remember_token' => str_random(10),
        'api_token' => str_random(60),
    ];
});
