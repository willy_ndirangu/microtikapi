<?php

namespace Seeders;

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds to seed the degree_classification table.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 1)->create();
    }
}