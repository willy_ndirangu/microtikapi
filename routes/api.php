<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/user', function (Request $request) {
//    return $request->user();
//})->middleware('auth:api');

Route::group(['middleware' => 'auth:api'], function () {

    Route::get('/user', 'Subscribers\SubscriberController@user');
    Route::get('/subscriber/unblock/{subscriber_unique_key}', 'Subscribers\SubscriberController@reconnect')->name('reconnect');
    Route::get('/error/unblock/{subscriber_unique_key}','Subscribers\SubscriberController@errorUnblocking')->name('error_unblocking');

});