<?php
namespace Repository;

use Models\Employee;


class EmployeeRepository
{

    public static function employeeEmails()
    {
        $employeeEmails = Employee::all()->map(function ($employee) {
            return $employee->email;
        })->toArray();
        return $employeeEmails;
    }

}