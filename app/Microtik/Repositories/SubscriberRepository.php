<?php
namespace Repository;


use Models\Subscriber;
use Carbon\Carbon;


class SubscriberRepository
{

    public static function getSubscriber($subscriber_unique_key)
    {
        return Subscriber::with('location')->where('subscriber_unique_key', $subscriber_unique_key)->get();

    }

    public static function UpdateSubscriberDisconnectionDate($subscriber_unique_key)
    {
        Subscriber::where('subscriber_unique_key', $subscriber_unique_key)->update(['connection_end_date' => Carbon::now()->addMonth()]);

    }

}