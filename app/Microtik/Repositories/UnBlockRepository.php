<?php

namespace Repository;

use Illuminate\Database\Eloquent\Collection;
use Models\Subscriber;
use Models\ConnectionStatus;
use PEAR2\Net\RouterOS;
use App\Notifications\Notify;
use App\Notifications\SlackNotification;
use ApiTrait\Microtik;

class UnBlockRepository
{

    use Microtik;

    /**
     * gets the subscriber to remove from the payment_reminder list in microtik
     * @param $unique_key -> subscriber unique identifier
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getSubscriber($unique_key)
    {
        $subscriber = Subscriber::with('location')->where('subscriber_unique_key', $unique_key)->first();
        return $subscriber;
    }


    /**
     * navigate to  firewall lists menu on  microtik
     * @param $routerOSClient
     * @return RouterOS\Util
     */
    public static function microtikUtilMenu($routerOSClient)
    {
        $routerOSUtil = new RouterOS\Util($routerOSClient);
        $routerOSUtil->setMenu('/ip firewall address-list');
        return $routerOSUtil;
    }

    /**
     * send command to remove a subscriber from payment reminder list
     * @param $subscriber_unique_key
     * @param $routerOSUtil
     * @param $subscriber
     * @param $slack
     * @param $fields
     */
    public static function enableInternetAccess($subscriber_unique_key, $routerOSUtil, $subscriber, $slack, $fields)
    {
        if ($routerOSUtil->remove(RouterOS\Query::where('address', $subscriber->ip))->getType() == RouterOS\Response::TYPE_FINAL) {
            $slack->notify(new SlackNotification("Reconnected", "#reconnect", 'Reconnect successful', $fields, true));
            self::updateSubscriber($subscriber_unique_key);
        } else {
            $slack->notify(new SlackNotification("Reconnect Error", "#reconnect", 'Reconnect Unsuccessful', $fields, false));

        }
    }


    /**
     * reconnect a subscriber
     * @param $subscriber_unique_key
     */
    public static function reconnect($subscriber_unique_key)
    {
        $subscriber = self::getSubscriber($subscriber_unique_key);
        $slack = new Notify();
        $fields = ['ip' => $subscriber->ip, 'Router' => $subscriber->location->router_ip, 'list' => 'Payment_reminder'];

        try {

            $routerOSClient = self::microtikLogin($subscriber->location->router_ip);
            $routerOSUtil = self::microtikUtilMenu($routerOSClient);
            self::enableInternetAccess($subscriber_unique_key, $routerOSUtil, $subscriber, $slack, $fields);


        } catch (\Exception $exception) {
            $fields = array_add($fields, 'exception', mb_strimwidth(((string)$exception), 0, 10, "..."));
            $slack->notify(new SlackNotification("Could Not Reconnect ", "#reconnect", 'Reconnect Fatal Error', $fields, false));

        }
    }



    /**
     * sets connection status of subscriber to connected
     * @param $subscriber_unique_key -> subscribers unique key
     */
    private static function updateSubscriber($subscriber_unique_key)
    {
        $connected = ConnectionStatus::where('status', 'connected')->first();
        if ($connected != null) {
            Subscriber::where('subscriber_unique_key', $subscriber_unique_key)->update(['connection_status_id' => $connected->id]);
            SubscriberRepository::UpdateSubscriberDisconnectionDate($subscriber_unique_key);
        }
    }


}