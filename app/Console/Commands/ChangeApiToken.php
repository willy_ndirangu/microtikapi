<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class ChangeApiToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api_token_change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'periodically change the api access token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::where('email','system@lastmile-networks.net')->update(['api_token'=>str_random(60)]);
    }
}
