<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Models\Subscriber;
use PEAR2\Net\RouterOS;
use Illuminate\Console\Command;
use App\Notifications\Notify;
use App\Notifications\SlackNotification;
use ApiTrait\Microtik;

class BlockNonPayedIP extends Command
{
    use Microtik;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blockIP';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add an ip to payment reminder list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $slack;

    public function __construct()
    {
        parent::__construct();
        $this->slack = new Notify();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $subscribers = Subscriber::with('location')->get();
        $subscribers->reject(function ($subscriber) {
            return Carbon::parse($subscriber->connection_end_date)->format('Y-m-d') != Carbon::now()->format('Y-m-d');

        })
            ->map(function ($subscriber) {
                $fields = ['ip' => $subscriber->ip, 'Router' => $subscriber->location->router_ip];

                try {

                    $routerOSClient = $this->microtikLogin($subscriber->location->router_ip);
                    $blockRequest = $this->blockCommand($subscriber);
                    $this->terminateSubscription($subscriber, $routerOSClient, $blockRequest, $fields);


                } catch (\Exception $exception) {
                    $this->slack->notify(new SlackNotification('Add Error', env('SLACK_BLOCK_CHANNEL'), 'unable to perform add', $fields, false));

                }

            });


    }


    /**
     * command to add a user to payment reminder list
     * @param $subscriber
     * @return RouterOS\Request
     */
    private function blockCommand($subscriber)
    {
        $blockRequest = new RouterOS\Request('/ip firewall address-list add  list="payment_reminder"');
        $blockRequest->setArgument('address', $subscriber->ip);
        return $blockRequest;
    }


    /**
     * sets connection status of subscriber to disconnected
     * @param $subscriber_unique_key -> subscribers unique key
     */
    private function updateSubscriber($subscriber_unique_key)
    {
        $connected = ConnectionStatus::where('status', 'disconnected')->first();
        if ($connected != null) {
            Subscriber::where('subscriber_unique_key', $subscriber_unique_key)->update(['connection_status_id' => $connected->id]);

        }
    }

    /**
     * Terminate subscription
     * @param $subscriber
     * @param $routerOSClient
     * @param $blockRequest
     * @param $fields
     */
    private function terminateSubscription($subscriber, $routerOSClient, $blockRequest, $fields)
    {
        if ($routerOSClient->sendSync($blockRequest)->getType() !== RouterOS\Response::TYPE_FINAL) {
            $this->slack->notify(new SlackNotification('Add unsuccessful', env('SLACK_BLOCK_CHANNEL'), "Could not add ip to the payment_reminder list", $fields, false));
        } else {
            $this->updateSubscriber($subscriber->subscriber_unique_key);
            $this->slack->notify(new SlackNotification(' Add successful  ', env('SLACK_BLOCK_CHANNEL'), "Added ip to the payment_reminder list", $fields, true));

        }
    }

}
