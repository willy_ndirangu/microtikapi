<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Models\Location;
use PEAR2\Net\RouterOS;
use App\Notifications\Notify;
use App\Notifications\SlackNotification;
use ApiTrait\Microtik;

class PingRouters extends Command
{
    use Microtik;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ping';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ping routers and check if they are up';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $slack;

    public function __construct()
    {
        parent::__construct();
        $this->slack = new Notify();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $routers = $this->getRoutersToPing();

        $routers->map(function ($router) {

            try {
                $routerOSClient = $this->microtikLogin($router->router_ip);
                $pingResults = $this->pingRequest($routerOSClient);
                $this->NotifyHighPacketLoss($router, $pingResults);
                $routerOSClient->close();

            } catch (\Exception $e) {
                $this->NotifyOnFailedConnection($router);

            }


        });
    }

    /**
     * ping microtik router
     * @param $routerOSClient
     * @return mixed
     */
    public function pingRequest($routerOSClient)
    {
        $pingRequest = new RouterOS\Request('/ping address=8.8.8.8 count=10');
        $pingResults = $routerOSClient->sendSync($pingRequest);
        return $pingResults;
    }

    /**
     * compose message for high packet loss
     * @param $routerIP
     * @param $pingResults
     * @return array
     */
    public function highPacketLoss($routerIP, $pingResults)
    {
        $fields = ['Router' => $routerIP, 'Packet loss' => $pingResults[9]->getProperty('packet-loss') . "%", 'avg-rtt' => $pingResults[9]->getProperty('avg-rtt')];
        return $fields;
    }

    /**
     * get routers to ping
     * @return mixed
     */
    private function getRoutersToPing()
    {
        return Location::select('router_ip')->groupBy('router_ip')->get();
    }

    /**
     * Notify on High Packet Loss
     * @param $router
     * @param $pingResults
     */
    private function NotifyHighPacketLoss($router, $pingResults)
    {
        if (intval($pingResults[9]->getProperty('packet-loss')) > 30) {
            $fields = $this->highPacketLoss($router->router_ip, $pingResults);
            $this->slack->notify(new SlackNotification('Router Ping Results', env('SLACK_CHANNEL'), "ping results notification", $fields, true));
        }
    }

    /**
     * Notify if connection to router fails
     * @param $router
     */
    private function NotifyOnFailedConnection($router)
    {
        $router = ['Router' => $router->router_ip];
        $this->slack->notify(new SlackNotification('Connection Error', env('SLACK_CHANNEL'), "Unable to connect to router", $router, false));
    }

}
