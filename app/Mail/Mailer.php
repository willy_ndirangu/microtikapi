<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Mailer extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * @var holds the email subject
     */
    protected $mailSubject;
    /**
     * @var holds the view for the email
     */
    protected $mailView;
    /**
     * @var array
     * Contains data to be rendered in the view
     */
    protected $mailData;


    /**
     * Mailer constructor.
     * @param null $subject -> email subject
     * @param $view -> email view
     * @param array $data -> email view data
     */
    public function __construct($subject = NULL, $view, $data = [])
    {
        $this->mailSubject = $subject;
        $this->mailView = $view;
        $this->mailData = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->mailView)
            ->with($this->mailData)
            ->subject($this->mailSubject);

    }
}
