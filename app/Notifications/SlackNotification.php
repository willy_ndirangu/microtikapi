<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;


class SlackNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * SlackNotification constructor.
     * @param $title ->title of the message for the fields
     * @param $channel -> channel to post to
     * @param $content -> the message body
     * @param $fields ->any fields for the message ; table like in slack
     */
    public $user;
    public $title;
    public $channel;
    public $content;
    public $fields;
    public $success;

    public function __construct($title, $channel, $content, $fields, $success)
    {
        //
        $this->user = new Notify();
        $this->content = $content;
        $this->title = $title;
        $this->fields = $fields;
        $this->channel = $channel;
        $this->success = $success;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }


    /**
     * send slack notifications
     * @param $notifiable
     * @return $this
     *
     */
    public function toSlack($notifiable)
    {
        if ($this->success == true) {
            return $this->successMessage();
        }

        return $this->errorMessage();


    }

    /**
     * success message for slack
     */
    public function successMessage()
    {
        return (new SlackMessage)
            ->success()
            ->content($this->content)
            ->from("System")
            ->to($this->channel)
            ->attachment(function ($attachment) {
                $attachment->title($this->title)
                    ->fields($this->fields);


            });
    }

    /**
     * error message for slack
     */
    public function errorMessage()
    {
        return (new SlackMessage)
            ->error()
            ->content($this->content)
            ->from("System")
            ->to($this->channel)
            ->attachment(function ($attachment) {
                $attachment->title($this->title)
                    ->fields($this->fields);


            });
    }

}
