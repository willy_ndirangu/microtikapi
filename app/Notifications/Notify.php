<?php
namespace App\Notifications;

use Illuminate\Notifications\Notifiable;

class Notify
{
    use Notifiable;

    /**
     * slack Notification web hook
     */
    public function routeNotificationForSlack()
    {
        return env('SLACK_HOOK');
    }
}