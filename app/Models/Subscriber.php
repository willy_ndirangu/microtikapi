<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    //table
    protected $table ='subscriber';

    //locations
    public function location()
    {
        return $this->hasOne('Models\Location','id','location_id');
    }
}
