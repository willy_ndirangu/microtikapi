<?php

namespace App\Http\Controllers\Subscribers;

use  App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Repository\UnBlockRepository;
use Repository\SubscriberRepository;
use Repository\EmployeeRepository;
use App\Mail\Mailer;

class SubscriberController extends Controller
{

    /**
     * reconnect user on paying monthly subscription
     * @param $subscriber_unique_key -> subscriber unique key
     */
    public function reconnect($subscriber_unique_key)
    {
        UnBlockRepository::reconnect($subscriber_unique_key);

    }

    public function user(Request $request)
    {
        return $request->user();

    }

    public function errorUnblocking($subscriber_unique_key)
    {
        $subscriber = SubscriberRepository::getSubscriber($subscriber_unique_key);
        $employeeEmails = EmployeeRepository::employeeEmails();
        $emailSubject = "Unable to reconnect " . $subscriber->first()->ip;
        $view = 'mails.reconnect_error';
        $viewData = ['data' => $subscriber->first()];
        Mail::to($employeeEmails)
            ->queue(new Mailer($emailSubject, $view, $viewData));
    }

}
